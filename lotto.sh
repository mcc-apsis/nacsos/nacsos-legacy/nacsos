#!/usr/bin/env bash

# list:
# crontab -u nacsos -l
# edit:
# crontab -u nacsos -e
# 30 11 * * 1-5 /var/www/nacsos1/lotto.sh

cd /var/www/nacsos1/tmv/BasicBrowser
source /var/www/nacsos1/venv/bin/activate
# run the django draw_lottery command but kill it after 30s if it doesn't stop
timeout -s 9 30 python manage.py draw_lottery
